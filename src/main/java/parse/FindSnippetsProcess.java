package parse;

import entities.FoundResult;
import entities.Query;
import entities.Snippet;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import javax.swing.*;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Vladislav on 24.10.2015.
 */
public class FindSnippetsProcess extends Thread {
    private static final Logger LOG = LogManager.getLogger(FindSnippetsProcess.class);
    private static final Pattern typicalTextInsideTagPattern = Pattern.compile("[^\\-\\+\\.,\\|:;\\\\/_]+");

    private final JProgressBar progressBar;
    private final Configuration hibernateConfig;

    private final String query;
    private final int snippetsRadius;
    private int pagesCount;

    private final List<EndProcessListener> listeners = new ArrayList<>();

    public FindSnippetsProcess(JProgressBar progressBar, Configuration hibernateConfig,
                               String query, int pagesCount, int snippetsRadius) {
        this.progressBar = progressBar;
        this.hibernateConfig = hibernateConfig;
        this.query = query;
        this.pagesCount = pagesCount;
        this.snippetsRadius = snippetsRadius;
    }

    public void addEndProcessListener(EndProcessListener endProcessListener) {
        listeners.add(endProcessListener);
    }

    @Override
    public void run() {
        progressBar.setMaximum(pagesCount);

        GoogleSession googleSession = null;
        try {
            SessionFactory sessionFactory = hibernateConfig.buildSessionFactory();
            Session hibernateSession = sessionFactory.openSession();
            hibernateSession.getTransaction().begin();
            Query insertedIntoDBQuery = new Query(query);
            hibernateSession.persist(insertedIntoDBQuery);
            hibernateSession.getTransaction().commit();

            googleSession = GoogleSession.getInstance();
            googleSession.openSession(query);
            while (pagesCount > 0) {
                List<FoundResult> foundItems = googleSession.getAllFoundItems();
                for (FoundResult foundItem : foundItems) {
                    try {
                        String pageUrl = foundItem.link;
                        String cachePageUrl = googleSession.getCacheTextPageLink(foundItem.link);

                        Document page = googleSession.sendGetAndReceivePage(pageUrl);
                        if (page == null) {
                            continue;
                        }
                        String pageText = page.text().trim().replaceAll("\u00A0", " ");

                        LOG.info("SNIPPETS={}", foundItem.snippets);
                        LOG.info("PAGETEXT={}", pageText);

                        int currentSnippetIndex = 0;
                        String description = "";
                        boolean descriptionFound = false;
                        Elements metaTags = page.select("meta");
                        if (!metaTags.isEmpty()) {
                            for (Element metaTag : metaTags) {
                                if (metaTag.attr("name").equals("description")) {
                                    String tagDescription = metaTag.attr("content");
                                    String firstSnippetMayBeDescription = foundItem.snippets.get(currentSnippetIndex);

                                    if (!tagDescription.trim().isEmpty()) {
                                        if (tagDescription.contains(firstSnippetMayBeDescription) ||
                                                firstSnippetMayBeDescription.contains(tagDescription)) {
                                            LOG.info("DESCRIPTION FOUND={}", tagDescription);
                                            description = tagDescription;
                                            descriptionFound = true;
                                        }
                                    }
                                }
                            }
                        }

                        boolean snippetFound = false;
                        for (int i = currentSnippetIndex; i < foundItem.snippets.size(); i++) {
                            String currentSnippet = foundItem.snippets.get(i);

                            ArrayList<String> snippetSentences = new ArrayList<>();
                            Matcher textMatcher = typicalTextInsideTagPattern.matcher(currentSnippet);
                            while (textMatcher.find()) {
                                String sentence = textMatcher.group().trim();
                                if (!sentence.isEmpty()) {
                                    snippetSentences.add(sentence);
                                    LOG.info("Sentence={}", sentence);
                                }
                            }

                            int resultStartIndex = -1;
                            int resultEndIndex = -1;
                            if (snippetSentences.size() == 1) {
                                String sentence = snippetSentences.get(0);
                                resultStartIndex = pageText.indexOf(sentence);
                                if (resultStartIndex < 0) {
                                    break;
                                }

                                resultStartIndex -= snippetsRadius;
                                resultStartIndex = resultStartIndex < 0 ? 0 : resultStartIndex;
                                resultEndIndex = resultStartIndex + sentence.length() + snippetsRadius;
                                resultEndIndex = resultEndIndex > pageText.length() ? pageText.length() : resultEndIndex;
                            } else if (snippetSentences.size() > 1) {
                                int startIndex = 0;
                                int endIndex = 0;

                                String firstSentence = snippetSentences.get(0);
                                String lastSentence = snippetSentences.get(snippetSentences.size() - 1);

                                LOG.info("First sentence={}", firstSentence);
                                LOG.info("Last sentence={}", lastSentence);
                                while (startIndex < pageText.length()) {
                                    startIndex = pageText.indexOf(firstSentence, startIndex);
                                    if (startIndex < 0) {
                                        LOG.info("first sentence not found");
                                        break;
                                    }
                                    LOG.info("Found input index={}", startIndex);

                                    endIndex = pageText.indexOf(lastSentence, startIndex + firstSentence.length());
                                    if (endIndex < 0) {
                                        LOG.info("last sentence not found");
                                        break;
                                    }
                                    LOG.info("Found last index={}", endIndex);

                                    String partOfPageMayBeWithSnippet = pageText.substring(startIndex, endIndex + lastSentence.length());
                                    boolean partContainsAllSubSentences = true;
                                    for (int sentenceIndex = 1; sentenceIndex < snippetSentences.size(); sentenceIndex++) {
                                        if (!partOfPageMayBeWithSnippet.contains(snippetSentences.get(sentenceIndex))) {
                                            partContainsAllSubSentences = false;
                                            break;
                                        }
                                    }

                                    if (partContainsAllSubSentences) {
                                        resultStartIndex = startIndex;
                                        resultEndIndex = endIndex;
                                        resultStartIndex -= snippetsRadius;
                                        resultStartIndex = resultStartIndex < 0 ? 0 : resultStartIndex;
                                        resultEndIndex = resultEndIndex + lastSentence.length() + snippetsRadius;
                                        resultEndIndex = resultEndIndex > pageText.length() ? pageText.length() : resultEndIndex;
                                        break;
                                    }
                                    startIndex++;
                                }
                            }

                            if (resultStartIndex >= 0 && resultEndIndex >= 0) {
                                while (Character.isLetter(pageText.charAt(resultStartIndex)) && resultStartIndex > 0) {
                                    resultStartIndex--;
                                }

                                while (Character.isLetter(pageText.charAt(resultEndIndex)) && resultEndIndex < pageText.length() - 1) {
                                    resultEndIndex++;
                                }

                                String snippetBody = pageText.substring(resultStartIndex, resultEndIndex);

                                persist(hibernateSession, new Snippet(insertedIntoDBQuery, currentSnippet, pageUrl, cachePageUrl,
                                        pageText, description, snippetBody, false));

                                snippetFound = true;
                            }
                        }

                        if (descriptionFound && !snippetFound) {
                            persist(hibernateSession, new Snippet(insertedIntoDBQuery, "", pageUrl, cachePageUrl,
                                    pageText, description, "", false));
                        }

                        pagesCount--;
                        progressBar.setValue(progressBar.getValue() + 1);
                    } catch (Exception e) {
                        LOG.error("Exception (might be normally", e);
                    }
                }

                if (googleSession.hashNextPage()) {
                    googleSession.nextPage();
                } else {
                    break;
                }
            }
        } catch (Exception ex) {
            LOG.error("Exception (might be normally)", ex);
        } finally {
            for (EndProcessListener listener : listeners) {
                listener.call();
            }

            if (googleSession != null) {
                googleSession.release();
            }
        }
    }

    private void persist(Session hibernateSession, Snippet snippet) {
        hibernateSession.clear();
        hibernateSession.getTransaction().begin();
        try {
            hibernateSession.persist(snippet);
            hibernateSession.flush();
            hibernateSession.getTransaction().commit();
        } catch (Throwable e) {
            hibernateSession.getTransaction().rollback();
        }
    }
}
