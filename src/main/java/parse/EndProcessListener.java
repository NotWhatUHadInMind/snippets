package parse;

/**
 * Created by Vladislav on 24.10.2015.
 */
public interface EndProcessListener {
    public void call();
}
