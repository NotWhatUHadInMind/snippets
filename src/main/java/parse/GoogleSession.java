package parse;

import entities.FoundResult;
import gui.MainForm;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.config.ConnectionConfig;
import org.apache.http.entity.ContentType;
import org.apache.http.impl.client.BasicCookieStore;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.openqa.selenium.By;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeDriverService;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;


/**
 * Created by Vladislav on 12.10.2015.
 */
public class GoogleSession {
    private static final Logger LOG = LogManager.getLogger(GoogleSession.class);

    private static final String USER_AGENT = "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/45.0.2454.101 Safari/537.36";
    private static final String CACHE_URL = "http://webcache.googleusercontent.com/search?q=cache:";
    private static final String TEXT_MODE = "&strip=1&vwsrc=0";
    private static final String SNIPPETS_DELIMITER = " \\.\\.\\. ";

    private static GoogleSession instance = new GoogleSession();

    private final HttpClient httpClient;
    private ChromeDriver driver;

    private Document currentPage;

    public static GoogleSession getInstance() {
        return instance;
    }

    private GoogleSession() {
        BasicCookieStore cookieStore = new BasicCookieStore();
        ConnectionConfig connectionConfig = ConnectionConfig.custom().setCharset(Charset.forName("UTF-8")).build();
        RequestConfig requestConfig = RequestConfig.custom().setSocketTimeout(5000).setConnectTimeout(5000).build();

        httpClient = HttpClientBuilder.create().
                setUserAgent(USER_AGENT).
                setDefaultRequestConfig(requestConfig).
                setDefaultConnectionConfig(connectionConfig).
                setDefaultCookieStore(cookieStore).
                build();
    }

    public void openSession(String query) throws IOException {
        ChromeDriverService driverService = new ChromeDriverService.Builder().
                usingAnyFreePort().withSilent(false).
                usingDriverExecutable(new File(System.getProperty(MainForm.CHROMEDRIVER_PATH_PROPERTY))).
                build();
        driver = new ChromeDriver(driverService);
        driver.get("https://www.google.com/");

        driver.findElement(By.cssSelector("#lst-ib")).sendKeys(query);
        driver.findElement(By.cssSelector("#sblsbb > button > span")).click();

        WebDriverWait webDriverWait = new WebDriverWait(driver, 30);
        webDriverWait.until(ExpectedConditions.elementToBeClickable(By.cssSelector("#center_col")));

        currentPage = Jsoup.parse(driver.getPageSource());
    }

    public boolean hashNextPage() {
        WebDriverWait webDriverWait = new WebDriverWait(driver, 10);
        webDriverWait.until(ExpectedConditions.elementToBeClickable(By.cssSelector("#pnnext > span.csb.ch")));
        return driver.findElement(By.cssSelector("#pnnext > span.csb.ch")) != null;
    }

    public void nextPage() throws IOException {
        WebDriverWait webDriverWait = new WebDriverWait(driver, 10);
        webDriverWait.until(ExpectedConditions.elementToBeClickable(By.cssSelector("#pnnext > span.csb.ch")));
        driver.findElement(By.cssSelector("#pnnext > span.csb.ch")).click();

        try {
            Thread.sleep(1000);//because chromedriver is unstable
        } catch (InterruptedException e) {
            LOG.error(e);
        }

        currentPage = Jsoup.parse(driver.getPageSource());
    }

    public List<FoundResult> getAllFoundItems() {
        List<FoundResult> result = new ArrayList<>();

        Elements containersGroups = currentPage.select("#rso div.srg");
        for (Element containersGroup : containersGroups) {
            Elements containers = containersGroup.select("> div.g > div.rc");
            for (Element container : containers) {
                try {
                    Elements h3Elements = container.select("> h3.r");
                    Elements divSElements = container.select("> div.s");
                    if (h3Elements.isEmpty() || divSElements.isEmpty()) {
                        continue;
                    }

                    Element pageHeader = h3Elements.get(0);
                    Element tagWithPageLink = pageHeader.select("> a").get(0);
                    String link = tagWithPageLink.attr("href");
                    String clearLink = link.substring(link.indexOf("http"), link.length());

                    Element snippetContainer = divSElements.select("span.st").get(0);
                    String snippetAsString = snippetContainer.text();

                    Elements dateContainers = snippetContainer.select(">span.f");
                    if (!dateContainers.isEmpty()) {
                        snippetAsString = snippetAsString.replaceFirst(Pattern.quote(dateContainers.get(0).text()), "");
                    }

                    String[] snippetParts = snippetAsString.split(SNIPPETS_DELIMITER);
                    for (int i = 0; i < snippetParts.length; i++) {
                        if (i == 0 && snippetParts[i].startsWith("...")) {
                            snippetParts[i] = snippetParts[i].substring(3);

                        } else if (i == snippetParts.length - 1 && snippetParts[i].endsWith("...")) {
                            snippetParts[i] = snippetParts[i].substring(0, snippetParts[i].length() - 3);
                        }

                        snippetParts[i] = snippetParts[i].replaceAll("\u00A0", " ");
                    }

                    LOG.info("link={}", clearLink);
                    LOG.info("snippetAsString={}", snippetAsString);
                    LOG.info("foundParts={}", Arrays.toString(snippetParts));

                    if (snippetParts.length > 0) {
                        result.add(new FoundResult(clearLink, Arrays.asList(snippetParts)));
                    }

                } catch (Exception e) {
                    LOG.error("Error during process find snippets on page", e);
                }
            }
        }

        if (result.isEmpty()) {
            LOG.info("Not found items, page={}", currentPage.html());
        }

        return result;
    }

    public void release() {
        if (driver != null) {
            driver.quit();
        }
    }

    public Document sendGetAndReceivePage(String query) throws IOException {
        LOG.info("Query = {}", query);
        HttpGet getRequest = new HttpGet(query);
        HttpResponse response;
        try {
            response = httpClient.execute(getRequest);
            HttpEntity entity = response.getEntity();
            Charset charset = ContentType.getOrDefault(entity).getCharset();
            if (charset != null) {
                return Jsoup.parse(EntityUtils.toString(entity, charset));
            } else {
                LOG.error("Cannot read response body");
                return null;
            }
        } finally {
            getRequest.releaseConnection();
        }
    }

    public String getCacheTextPageLink(String link) {
        return CACHE_URL + link + TEXT_MODE;
    }

}
