package gui;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.cfg.Configuration;
import org.openqa.selenium.chrome.ChromeDriverService;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

/**
 * Created by Vladislav on 20.10.2015.
 */
public class SettingsForm {
    public static final String INVALID_HIBERNATE_SETTING_MESSAGE = "Невалидные настройки базы данных";
    public static final String INVALID_CHROMEDRIVER_SETTING_MESSAGE = "Невалидные настройки chromedriver";
    public static final String ERROR_DIALOG_TITLE = "Ошибка";

    private static final Logger LOG = LogManager.getLogger(SettingsForm.class);
    private static final String SETTINGS_FORM_TITLE = "Настройки";

    private JTextField urlField;
    private JButton acceptButton;
    private JTextField userField;
    private JTextField passwordField;
    private JTextField chromeDriverField;

    public JPanel mainPanel;
    public JFrame frame;

    public Configuration hibernateConfig;

    public SettingsForm(Configuration hibernateConfig) {
        this.hibernateConfig = hibernateConfig;
        resetHibernateSettingsFields();
        resetChromedriverSettingsFields();

        frame = new JFrame(SETTINGS_FORM_TITLE);
        frame.setContentPane(mainPanel);
        frame.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
        frame.pack();
        frame.setLocationRelativeTo(null);

        acceptButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    String url = urlField.getText();
                    String user = userField.getText();
                    String password = passwordField.getText();

                    Configuration notCheckedConfig = new Configuration ().
                            configure(System.getProperty(MainForm.HIBERNATE_CONFIG_PROPERTY)).
                            setProperty("hibernate.connection.url", url).
                            setProperty("hibernate.connection.username", user).
                            setProperty("hibernate.connection.password", password);
                    notCheckedConfig.buildSessionFactory();

                    SettingsForm.this.hibernateConfig = notCheckedConfig;
                } catch (Exception ex) {
                    LOG.error("Invalid hibernate settings", ex);
                    JOptionPane.showMessageDialog(mainPanel, INVALID_HIBERNATE_SETTING_MESSAGE, ERROR_DIALOG_TITLE, JOptionPane.ERROR_MESSAGE);
                    resetHibernateSettingsFields();
                }

                try {
                    String chromeDriverPath = chromeDriverField.getText();
                    new ChromeDriverService.Builder().usingAnyFreePort().withSilent(true).
                            usingDriverExecutable(new File(chromeDriverPath)).
                            build();

                    System.setProperty(MainForm.CHROMEDRIVER_PATH_PROPERTY, chromeDriverPath);
                } catch (Exception ex) {
                    LOG.error("Invalid chromedriver settings", ex);
                    JOptionPane.showMessageDialog(mainPanel, INVALID_CHROMEDRIVER_SETTING_MESSAGE, ERROR_DIALOG_TITLE, JOptionPane.ERROR_MESSAGE);
                    resetChromedriverSettingsFields();
                }
            }
        });
    }

    private void resetHibernateSettingsFields() {
        if (hibernateConfig != null) {
            urlField.setText(hibernateConfig.getProperty("hibernate.connection.url"));
            userField.setText(hibernateConfig.getProperty("hibernate.connection.username"));
            passwordField.setText(hibernateConfig.getProperty("hibernate.connection.password"));
        }
    }

    private void resetChromedriverSettingsFields() {
        chromeDriverField.setText(System.getProperty(MainForm.CHROMEDRIVER_PATH_PROPERTY));
    }
}
