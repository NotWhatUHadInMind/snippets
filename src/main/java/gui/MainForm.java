package gui;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.cfg.Configuration;
import parse.EndProcessListener;
import parse.FindSnippetsProcess;
import parse.GoogleSession;

import javax.swing.*;
import javax.swing.text.DefaultFormatterFactory;
import javax.swing.text.NumberFormatter;
import java.awt.event.*;
import java.io.File;
import java.text.NumberFormat;

/**
 * Created by Vladislav on 10.10.2015.
 */
public class MainForm {
    public static final String CHROMEDRIVER_PATH_PROPERTY = "webdriver.chrome.driver";
    public static final String HIBERNATE_CONFIG_PROPERTY = "hibernateConfig";

    private static final Logger LOG = LogManager.getLogger(MainForm.class);
    private static final String MAIN_FORM_TITLE = "Поиск сниппетов";

    private JPanel mainPanel;
    private JButton findButton;
    private JFormattedTextField searchField;
    private JProgressBar progressBar;
    private JFormattedTextField pagesCountField;
    private JFormattedTextField snippetsRadiusField;
    private JButton settingsButton;

    private Configuration configuration;

    private static JFrame mainFrame;
    private SettingsForm databaseSettingsForm;

    public static void main(String[] args) {
        System.setProperty(CHROMEDRIVER_PATH_PROPERTY, "/chromedriver.exe");
        System.setProperty(HIBERNATE_CONFIG_PROPERTY, "/hibernate.cfg.xml");
        System.setProperty("file.encoding", "UTF-8");

        mainFrame = new JFrame(MAIN_FORM_TITLE);
        mainFrame.setContentPane(new MainForm().mainPanel);
        mainFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        mainFrame.pack();
        mainFrame.setLocationRelativeTo(null);
        mainFrame.setVisible(true);

        mainFrame.addWindowListener(new WindowListener() {
            @Override
            public void windowOpened(WindowEvent e) {
            }

            @Override
            public void windowClosing(WindowEvent e) {
                GoogleSession.getInstance().release();
            }

            @Override
            public void windowClosed(WindowEvent e) {
            }

            @Override
            public void windowIconified(WindowEvent e) {
            }

            @Override
            public void windowDeiconified(WindowEvent e) {
            }

            @Override
            public void windowActivated(WindowEvent e) {
            }

            @Override
            public void windowDeactivated(WindowEvent e) {
            }
        });
    }

    public MainForm() {
        final NumberFormat format = NumberFormat.getInstance();
        format.setGroupingUsed(false);
        NumberFormatter formatter = new NumberFormatter(format);
        formatter.setValueClass(Integer.class);
        formatter.setMinimum(0);
        formatter.setMaximum(Integer.MAX_VALUE);
        formatter.setCommitsOnValidEdit(true);

        pagesCountField.setFormatterFactory(new DefaultFormatterFactory(formatter));
        pagesCountField.setValue(1);

        snippetsRadiusField.setFormatterFactory(new DefaultFormatterFactory(formatter));
        snippetsRadiusField.setValue(0);

        findButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                findButton.setEnabled(false);
                settingsButton.setEnabled(false);

                String query = searchField.getText();
                int pagesCount = (int)pagesCountField.getValue();
                int snippetsRadius = (int)snippetsRadiusField.getValue();

                FindSnippetsProcess findSnippetsProcess = new FindSnippetsProcess(progressBar, configuration,
                        query, pagesCount, snippetsRadius);
                findSnippetsProcess.addEndProcessListener(new EndProcessListener() {
                    @Override
                    public void call() {
                        progressBar.setValue(0);
                        findButton.setEnabled(true);
                        settingsButton.setEnabled(true);
                    }
                });
                findSnippetsProcess.start();
            }
        });

        try {
            configuration = new Configuration().configure(System.getProperty(HIBERNATE_CONFIG_PROPERTY));
            configuration.buildSessionFactory();
        } catch (HibernateException e) {
            LOG.error("Invalid hibernate settings", e);
            JOptionPane.showMessageDialog(mainPanel, SettingsForm.INVALID_HIBERNATE_SETTING_MESSAGE,
                    SettingsForm.ERROR_DIALOG_TITLE, JOptionPane.ERROR_MESSAGE);
        }

        if (!new File(System.getProperty(CHROMEDRIVER_PATH_PROPERTY)).exists()) {
            JOptionPane.showMessageDialog(mainPanel, SettingsForm.INVALID_CHROMEDRIVER_SETTING_MESSAGE,
                    SettingsForm.ERROR_DIALOG_TITLE, JOptionPane.ERROR_MESSAGE);
        }

        databaseSettingsForm = new SettingsForm(configuration);
        databaseSettingsForm.frame.addWindowListener(new WindowListener() {
            @Override
            public void windowOpened(WindowEvent e) {
            }

            @Override
            public void windowClosing(WindowEvent e) {
                configuration = databaseSettingsForm.hibernateConfig;
                databaseSettingsForm.frame.setVisible(false);
                mainFrame.setEnabled(true);
                mainFrame.setVisible(true);
            }

            @Override
            public void windowClosed(WindowEvent e) {
            }

            @Override
            public void windowIconified(WindowEvent e) {
            }

            @Override
            public void windowDeiconified(WindowEvent e) {
            }

            @Override
            public void windowActivated(WindowEvent e) {
            }

            @Override
            public void windowDeactivated(WindowEvent e) {
            }
        });

        settingsButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                mainFrame.setEnabled(false);
                databaseSettingsForm.frame.setVisible(true);
            }
        });
    }
}
