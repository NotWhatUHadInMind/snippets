package entities;

import javax.persistence.*;

/**
 * Created by Vladislav on 11.10.2015.
 */

@Entity
@Table(name = "query")
public class Query {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "queryBody")
    private String body;

    public Query() {
    }

    public Query(String body) {
        this.body = body;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    @Override
    public String toString() {
        return "Query{" +
                "id=" + id +
                ", body='" + body + '\'' +
                '}';
    }
}
