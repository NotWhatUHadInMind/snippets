package entities;


import java.util.List;

/**
 * Created by Vladislav on 17.10.2015.
 */
public class FoundResult {
    public final String link;
    public final List<String> snippets;

    public FoundResult(String link, List<String> snippets) {
        this.link = link;
        this.snippets = snippets;
    }

    @Override
    public String toString() {
        return "FoundResult{" +
                "link='" + link + '\'' +
                ", snippets=" + snippets +
                '}';
    }
}
